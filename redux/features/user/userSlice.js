import { loadUser, login, logout, register } from "./userApi";
import { HYDRATE } from "next-redux-wrapper";
const { createAsyncThunk, createSlice } = require("@reduxjs/toolkit");

const initialState = {
    user: {},
    isAuth: false,
    isError: false,
    isLoading: false,
    error: null,
};
export const registerUser = createAsyncThunk(
    "user/registerUser",
    async (userData) => {
        const user = await register(userData);

        return user;
    }
);
export const loginUser = createAsyncThunk(
    "user/loginUser",
    async (loginData) => {
        const user = await login(loginData);

        return user;
    }
);

export const loadLoginUser = createAsyncThunk("user/loadUser", async () => {
    const user = await loadUser();

    return user;
});

export const logoutUser = createAsyncThunk("user/logout", async () => {
    const user = await logout();

    return user;
});

// create slice

const userSlice = createSlice({
    name: "user",
    initialState,
    reducers: {
        clearError: (state, action) => {
            state.error = null;
            state.isError = false;
        },
        getLoginData: (state, action) => {
            state.isError = false;
            state.isLoading = false;
            state.isAuth = true;
            state.error = null;
            state.user = action.payload;
        },
    },

    extraReducers: (builder) => {
        builder
            .addCase(registerUser.pending, (state, action) => {
                state.isLoading = true;
            })
            .addCase(registerUser.fulfilled, (state, action) => {
                if (action.payload?.error?.success == false) {
                    state.isError = true;
                    state.isLoading = false;
                    state.isAuth = false;
                    state.user = {};
                    state.error = action.payload?.error?.message;
                } else {
                    state.isError = false;
                    state.isLoading = false;
                    state.isAuth = true;
                    state.user = action.payload;
                    state.error = null;
                }
            })
            .addCase(registerUser.rejected, (state, action) => {
                state.isLoading = false;
                state.isError = true;
                state.error = action.error?.message;
                state.isAuth = false;
                state.user = {};
            })
            .addCase(loginUser.pending, (state, action) => {
                state.isLoading = true;
                state.isError = false;
                state.error = null;
            })
            .addCase(loginUser.fulfilled, (state, action) => {
                if (action.payload?.error) {
                    state.isError = true;
                    state.isLoading = false;
                    state.isAuth = false;
                    state.error = action.payload?.error;
                    state.user = {};
                } else {
                    state.isError = false;
                    state.isLoading = false;
                    state.isAuth = true;
                    state.error = null;
                    state.user = action.payload;
                }
            })
            .addCase(loginUser.rejected, (state, action) => {
                state.isLoading = false;
                state.isError = true;
                state.error = action.error?.message;
                state.isAuth = false;
                state.user = {};
            })
            .addCase(loadLoginUser.pending, (state, action) => {
                state.isLoading = true;
            })
            .addCase(loadLoginUser.fulfilled, (state, action) => {
                if (action.payload.error) {
                    state.isError = true;
                    state.isLoading = false;
                    state.isAuth = false;

                    state.error = action.payload.error;
                } else {
                    state.isError = false;
                    state.isLoading = false;
                    state.isAuth = true;
                    state.error = "";
                    state.user = action.payload;
                }
            })
            .addCase(loadLoginUser.rejected, (state, action) => {
                state.isLoading = false;
                state.isError = true;
                state.error = action.error?.message;
                state.isAuth = false;
                state.user = {};
            })
            .addCase(logoutUser.pending, (state, action) => {
                state.isLoading = true;
            })
            .addCase(logoutUser.fulfilled, (state, action) => {
                state.isError = false;
                state.isLoading = false;
                state.isAuth = false;
                state.user = {};
            })
            .addCase(logoutUser.rejected, (state, action) => {
                state.isLoading = false;
                state.isError = true;
                state.error = action.error?.message;
                state.isAuth = false;
                state.user = {};
            });
    },
});

export default userSlice.reducer;
export const { clearError, getLoginData } = userSlice.actions;
