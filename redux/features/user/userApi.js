import axios from "../../../utils/axios";

export const register = async (userData) => {
    try {
        const config = {
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "*",
            },
        };
        axios.defaults.withCredentials = true;

        const res = await axios.post(`/plan/v1/register`, userData, config);

        return res.data;
    } catch (error) {
        return { error: error?.response?.data?.message };
    }
};

export const loadUser = async () => {
    try {
        const config = {
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "*",
            },
        };
        axios.defaults.withCredentials = true;

        const { data } = await axios.get(`/plan/v1/me`, config);

        return data;
    } catch (error) {
        return { error: error?.response?.data?.message };
    }
};

export const login = async (loginData) => {
    try {
        console.log("data");
        const config = {
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "*",
            },
        };
        axios.defaults.withCredentials = true;

        const { data } = await axios.post(`/plan/v1/login`, loginData, config);

        return data;
    } catch (error) {
        console.log("error here", error);
        return { error: error?.response?.data?.message };
    }
};

export const logout = async () => {
    try {
        await axios.get(`/plan/v1/logout`);
    } catch (error) {
        return error.response?.data?.message;
    }
};
