import axios from "../../../utils/axios";

export const createTaskApi = async (taskData) => {
    try {
        const config = {
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "*",
            },
        };
        axios.defaults.withCredentials = true;

        const { data } = await axios.post(
            `/plan/v1/create-task`,
            taskData,
            config
        );

        return data;
    } catch (error) {
        console.log(error);
        return { error: error.response.data.message };
    }
};

export const getAllTaskApi = async () => {
    try {
        const config = {
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "*",
            },
        };
        axios.defaults.withCredentials = true;

        const { data } = await axios.get(`/plan/v1/tasks`, config);

        return data;
    } catch (error) {
        console.log(error);
        return { error: error.response.data.message };
    }
};

export const getFilterTasksApi = async (checks) => {
    try {
        const config = {
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "*",
            },
        };
        axios.defaults.withCredentials = true;

        let url = `/plan/v1/filtered-tasks?completed=${checks.completed}&upcomming=${checks.upcomming}&low=${checks.low}&medium=${checks.medium}&high=${checks.high}`;

        const { data } = await axios.get(url, config);

        return data;
    } catch (error) {
        console.log(error);
        return { error: error.response.data.message };
    }
};

export const taskDoneApi = async (id) => {
    try {
        const config = {
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "*",
            },
        };
        axios.defaults.withCredentials = true;

        const { data } = await axios.put(`/plan/v1/task/${id}`, config);

        return data;
    } catch (error) {
        console.log(error);
        return { error: error.response.data.message };
    }
};

export const taskDeleteApi = async (id) => {
    try {
        const config = {
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Headers": "*",
            },
        };
        axios.defaults.withCredentials = true;

        const { data } = await axios.delete(`/plan/v1/task/${id}`, config);

        return data;
    } catch (error) {
        console.log(error);
        return { error: error.response.data.message };
    }
};
