import {
    createTaskApi,
    getAllTaskApi,
    getFilterTasksApi,
    taskDeleteApi,
    taskDoneApi,
} from "./taskApi";

const { createAsyncThunk, createSlice } = require("@reduxjs/toolkit");

const initialState = {
    allTasks: [],
    task: {},
    isError: false,
    isLoading: false,
    error: null,
    selectedTask: {},
};
export const createTask = createAsyncThunk(
    "task/createTask",
    async (taskData) => {
        const user = await createTaskApi(taskData);

        return user;
    }
);

export const getTasks = createAsyncThunk("task/getTasks", async () => {
    const user = await getAllTaskApi();

    return user;
});

export const getFilterTasks = createAsyncThunk(
    "task/getFilterTasks",
    async (checks) => {
        const tasks = await getFilterTasksApi(checks);

        return tasks;
    }
);

export const taskDone = createAsyncThunk("task/taskDone", async (id) => {
    const user = await taskDoneApi(id);

    return user;
});

export const deleteTask = createAsyncThunk("task/deleteTask", async (id) => {
    const user = await taskDeleteApi(id);

    return user;
});
// create slice

const taskSlice = createSlice({
    name: "task",
    initialState,
    reducers: {
        clearError: (state, action) => {
            state.error = null;
            state.isError = false;
        },
        selectTask: (state, action) => {
            state.selectedTask = action.payload;
        },
    },
    extraReducers: (builder) => {
        builder
            .addCase(createTask.pending, (state, action) => {
                state.isLoading = true;
            })
            .addCase(createTask.fulfilled, (state, action) => {
                if (action.payload?.error?.success == false) {
                    state.isError = true;
                    state.isLoading = false;
                    state.task = {};
                    state.error = action.payload?.error?.message;
                } else {
                    state.isError = false;
                    state.isLoading = false;
                    state.task = action.payload;
                    state.error = null;
                }
            })
            .addCase(createTask.rejected, (state, action) => {
                state.isLoading = false;
                state.isError = true;
                state.error = action.error?.message;
                state.task = {};
            })
            .addCase(getTasks.pending, (state, action) => {
                state.isLoading = true;
            })
            .addCase(getTasks.fulfilled, (state, action) => {
                if (action.payload?.error?.success == false) {
                    state.isError = true;
                    state.isLoading = false;
                    state.error = action.payload?.error?.message;
                } else {
                    state.isError = false;
                    state.isLoading = false;
                    state.allTasks = action.payload.tasks;
                    state.error = null;
                }
            })
            .addCase(getTasks.rejected, (state, action) => {
                state.isLoading = false;
                state.isError = true;
                state.error = action.error?.message;
                state.task = {};
            })
            .addCase(getFilterTasks.pending, (state, action) => {
                state.isLoading = true;
            })
            .addCase(getFilterTasks.fulfilled, (state, action) => {
                if (action.payload?.error?.success == false) {
                    state.isError = true;
                    state.isLoading = false;
                    state.error = action.payload?.error?.message;
                } else {
                    state.isError = false;
                    state.isLoading = false;
                    state.allTasks = action.payload.tasks;
                    state.error = null;
                }
            })
            .addCase(getFilterTasks.rejected, (state, action) => {
                state.isLoading = false;
                state.isError = true;
                state.error = action.error?.message;
                state.allTasks = [];
            })
            .addCase(taskDone.pending, (state, action) => {
                state.isLoading = true;
            })
            .addCase(taskDone.fulfilled, (state, action) => {
                if (action.payload?.error?.success == false) {
                    state.isError = true;
                    state.isLoading = false;
                    state.error = action.payload?.error?.message;
                } else {
                    const newArr = [...state.allTasks];
                    const arr = newArr.map((e) => {
                        if (action.payload.updatedTask._id == e._id)
                            return {
                                ...action.payload.updatedTask,
                            };
                        return {
                            ...e,
                        };
                    });

                    state.isError = false;
                    state.isLoading = false;
                    state.task = action.payload;
                    state.error = null;
                    state.allTasks = arr;
                    state.selectedTask = action.payload.updatedTask;
                }
            })
            .addCase(taskDone.rejected, (state, action) => {
                state.isLoading = false;
                state.isError = true;
                state.error = action.error?.message;
                state.task = {};
            })
            .addCase(deleteTask.pending, (state, action) => {
                state.isLoading = true;
            })
            .addCase(deleteTask.fulfilled, (state, action) => {
                if (action.payload?.error?.success == false) {
                    state.isError = true;
                    state.isLoading = false;
                    state.task = {};
                    state.error = action.payload?.error?.message;
                } else {
                    state.isError = false;
                    state.isLoading = false;
                    state.error = null;
                }
            })
            .addCase(deleteTask.rejected, (state, action) => {
                state.isLoading = false;
                state.isError = true;
                state.error = action.error?.message;
            });
    },
});

export default taskSlice.reducer;
export const { clearError, selectTask } = taskSlice.actions;
