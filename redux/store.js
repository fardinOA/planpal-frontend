import { configureStore } from "@reduxjs/toolkit";
import { createWrapper } from "next-redux-wrapper";
import userReducer from "./features/user/userSlice";
import taskReducer from "./features/task/taskSlice";
const makeStore = () =>
    configureStore({
        reducer: {
            user: userReducer,
            task: taskReducer,
        },
    });

export const wrapper = createWrapper(makeStore);
