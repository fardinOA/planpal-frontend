import { useRouter } from "next/router";
import { useSelector } from "react-redux";
import Navbar from "./Navbar";

export default function Layout({ children }) {
    const { asPath } = useRouter();
    // console.log(asPath);

    return (
        <>
            {["/login", "/register"].includes(asPath) ? null : <Navbar />}

            <main className="  ">{children}</main>
        </>
    );
}
