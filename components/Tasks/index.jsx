import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Loader from "../Loader";
import {
    getFilterTasks,
    selectTask,
} from "../../redux/features/task/taskSlice";

const Tasks = () => {
    const dispatch = useDispatch();
    const [showFilters, setShowfilters] = useState(false);
    const [check, setCheck] = useState({
        completed: false,
        upcomming: false,
        low: false,
        medium: false,
        high: false,
    });
    const { completed, upcomming, low, medium, high } = check;

    const changeHandler = (e) => {
        setCheck({
            ...check,
            [e.target.name]: e.target.checked,
        });
    };
    const { isLoading, allTasks } = useSelector((state) => state.task);

    const applyFilters = (e) => {
        e.preventDefault();
        dispatch(getFilterTasks(check));
    };
    // console.log(check);
    if (isLoading) return <Loader />;

    return (
        <div className=" flex justify-center items-center   ">
            <div>
                <div>
                    <div className="2xl:container 2xl:mx-auto">
                        <div className=" md:py-12  md:px-6 py-9 px-4">
                            <div className=" flex justify-between   items-center mb-4">
                                <h2 className=" lg:text-4xl text-3xl lg:leading-9 leading-7 text-gray-800 font-semibold">
                                    Task Filter
                                </h2>

                                <button
                                    onClick={() => setShowfilters(!showFilters)}
                                    className=" cursor-pointer sm:flex hidden hover:bg-gray-700 focus:ring focus:ring-2 focus:ring-offset-2 focus:ring-gray-800 py-4 px-6 bg-gray-800 flex text-base leading-4 font-normal text-white justify-center items-center "
                                >
                                    <svg
                                        className=" mr-2"
                                        width="24"
                                        height="24"
                                        viewBox="0 0 24 24"
                                        fill="none"
                                        xmlns="http://www.w3.org/2000/svg"
                                    >
                                        <path
                                            d="M6 12C7.10457 12 8 11.1046 8 10C8 8.89543 7.10457 8 6 8C4.89543 8 4 8.89543 4 10C4 11.1046 4.89543 12 6 12Z"
                                            stroke="white"
                                            strokeWidth="1.5"
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                        />
                                        <path
                                            d="M6 4V8"
                                            stroke="white"
                                            strokeWidth="1.5"
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                        />
                                        <path
                                            d="M6 12V20"
                                            stroke="white"
                                            strokeWidth="1.5"
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                        />
                                        <path
                                            d="M12 18C13.1046 18 14 17.1046 14 16C14 14.8954 13.1046 14 12 14C10.8954 14 10 14.8954 10 16C10 17.1046 10.8954 18 12 18Z"
                                            stroke="white"
                                            strokeWidth="1.5"
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                        />
                                        <path
                                            d="M12 4V14"
                                            stroke="white"
                                            strokeWidth="1.5"
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                        />
                                        <path
                                            d="M12 18V20"
                                            stroke="white"
                                            strokeWidth="1.5"
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                        />
                                        <path
                                            d="M18 9C19.1046 9 20 8.10457 20 7C20 5.89543 19.1046 5 18 5C16.8954 5 16 5.89543 16 7C16 8.10457 16.8954 9 18 9Z"
                                            stroke="white"
                                            strokeWidth="1.5"
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                        />
                                        <path
                                            d="M18 4V5"
                                            stroke="white"
                                            strokeWidth="1.5"
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                        />
                                        <path
                                            d="M18 9V20"
                                            stroke="white"
                                            strokeWidth="1.5"
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                        />
                                    </svg>
                                    Filters
                                </button>
                            </div>
                            <p className=" text-xl leading-5 text-gray-600 font-medium">
                                {allTasks?.length} Tasks
                            </p>

                            {/* Filters Button (Small Screen)  */}

                            <button
                                onClick={() => setShowfilters(!showFilters)}
                                className="cursor-pointer mt-6 block sm:hidden hover:bg-gray-700 focus:ring focus:ring-2 focus:ring-offset-2 focus:ring-gray-800 py-2 w-full bg-gray-800 flex text-base leading-4 font-normal text-white justify-center items-center"
                            >
                                <svg
                                    className=" mr-2"
                                    width="24"
                                    height="24"
                                    viewBox="0 0 24 24"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                >
                                    <path
                                        d="M6 12C7.10457 12 8 11.1046 8 10C8 8.89543 7.10457 8 6 8C4.89543 8 4 8.89543 4 10C4 11.1046 4.89543 12 6 12Z"
                                        stroke="white"
                                        strokeWidth="1.5"
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                    />
                                    <path
                                        d="M6 4V8"
                                        stroke="white"
                                        strokeWidth="1.5"
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                    />
                                    <path
                                        d="M6 12V20"
                                        stroke="white"
                                        strokeWidth="1.5"
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                    />
                                    <path
                                        d="M12 18C13.1046 18 14 17.1046 14 16C14 14.8954 13.1046 14 12 14C10.8954 14 10 14.8954 10 16C10 17.1046 10.8954 18 12 18Z"
                                        stroke="white"
                                        strokeWidth="1.5"
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                    />
                                    <path
                                        d="M12 4V14"
                                        stroke="white"
                                        strokeWidth="1.5"
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                    />
                                    <path
                                        d="M12 18V20"
                                        stroke="white"
                                        strokeWidth="1.5"
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                    />
                                    <path
                                        d="M18 9C19.1046 9 20 8.10457 20 7C20 5.89543 19.1046 5 18 5C16.8954 5 16 5.89543 16 7C16 8.10457 16.8954 9 18 9Z"
                                        stroke="white"
                                        strokeWidth="1.5"
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                    />
                                    <path
                                        d="M18 4V5"
                                        stroke="white"
                                        strokeWidth="1.5"
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                    />
                                    <path
                                        d="M18 9V20"
                                        stroke="white"
                                        strokeWidth="1.5"
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                    />
                                </svg>
                                Filters
                            </button>
                        </div>

                        <div
                            id="filterSection"
                            className={
                                "relative md:py-10 lg:px-20 md:px-6 py-9 px-4 bg-gray-50 w-full " +
                                (showFilters ? "block" : "hidden")
                            }
                        >
                            {/* Cross button Code  */}
                            <div
                                onClick={() => setShowfilters(false)}
                                className=" cursor-pointer absolute right-0 top-0 md:py-10 lg:px-20 md:px-6 py-9 px-4"
                            >
                                <svg
                                    className=" lg:w-6 lg:h-6 w-4 h-4"
                                    viewBox="0 0 26 26"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                >
                                    <path
                                        d="M25 1L1 25"
                                        stroke="#1F2937"
                                        strokeWidth="1.25"
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                    />
                                    <path
                                        d="M1 1L25 25"
                                        stroke="#27272A"
                                        strokeWidth="1.25"
                                        strokeLinecap="round"
                                        strokeLinejoin="round"
                                    />
                                </svg>
                            </div>
                            <hr className=" bg-gray-200 lg:w-6/12 w-full md:my-10 my-8" />
                            {/* Colors Section */}
                            <div>
                                <div className=" flex items-center justify-between w-[8rem] gap-4 ">
                                    <label htmlFor="">Completed</label>
                                    <input
                                        className="w-4 h-4 mr-2"
                                        type="checkbox"
                                        id="completed"
                                        name="completed"
                                        checked={check.completed}
                                        onChange={changeHandler}
                                    />
                                </div>
                                <div className=" flex items-center justify-between w-[8rem] gap-4 ">
                                    <label htmlFor="">Upcomming</label>
                                    <input
                                        className="w-4 h-4 mr-2"
                                        type="checkbox"
                                        id="upcomming"
                                        name="upcomming"
                                        onChange={changeHandler}
                                        checked={check.upcomming}
                                    />
                                </div>
                            </div>
                            <hr className=" bg-gray-200 lg:w-6/12 w-full md:my-10 my-8" />
                            <div className=" flex space-x-6 justify-center items-center">
                                <label
                                    className=" mr-2 text-[1rem] leading-3 font-normal text-gray-600"
                                    htmlFor="Leather"
                                >
                                    Priority
                                </label>
                            </div>

                            <div>
                                <div className=" w-[7rem] flex items-center justify-between gap-4 ">
                                    <label htmlFor="">Low</label>
                                    <input
                                        className="w-4 h-4 mr-2  "
                                        type="checkbox"
                                        id="low"
                                        name="low"
                                        onChange={changeHandler}
                                        checked={check.low}
                                    />
                                </div>
                                <div className=" w-[7rem] flex items-center justify-between gap-4 ">
                                    <label htmlFor="">Medium</label>
                                    <input
                                        className="w-4 h-4 mr-2"
                                        type="checkbox"
                                        id="medium"
                                        name="medium"
                                        onChange={changeHandler}
                                        checked={check.medium}
                                    />
                                </div>
                                <div className=" w-[7rem] flex items-center justify-between gap-4 ">
                                    <label htmlFor="">High</label>
                                    <input
                                        className="w-4 h-4 mr-2"
                                        type="checkbox"
                                        id="high"
                                        name="high"
                                        onChange={changeHandler}
                                        checked={check.high}
                                    />
                                </div>
                            </div>

                            <hr className=" bg-gray-200 lg:w-6/12 w-full md:my-10 my-8" />

                            <div className="px-0 mt-10 w-full md:w-auto md:mt-0 md:absolute md:right-0 md:bottom-0 md:py-10 lg:px-20 md:px-6">
                                <button
                                    onClick={applyFilters}
                                    className="w-full hover:bg-gray-700 focus:ring focus:ring-offset-2 focus:ring-gray-800 text-base leading-4 font-medium py-4 px-10 text-white bg-gray-800"
                                >
                                    Apply Filter
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className=" px-4 flex flex-col   gap-8 mt-5 w-full h-full ">
                    {allTasks?.map((ele, ind) => (
                        <label
                            onClick={() => {
                                dispatch(selectTask(ele));
                            }}
                            htmlFor="my-modal-5"
                            key={ele._id}
                            className={` hover:-translate-y-1  cursor-pointer relative bg-green-200 p-4 w-[20rem] md:w-[25rem] transition-all duration-300 rounded-md shadow-lg mx-auto`}
                        >
                            {/* <label className="btn">open modal</label> */}

                            <h1 className=" text-[1.3rem] ">{ele.title}</h1>
                            <h3 className=" capitalize ">
                                Due On:{" "}
                                {Date(ele.dueDate)
                                    .toLocaleLowerCase()
                                    .slice(0, 16)}
                            </h3>

                            {!ele.completed &&
                                new Date(ele.dueDate).toDateString() ===
                                    new Date().toDateString() && (
                                    <p className="absolute top-0 right-0 rounded-tr-md text-white bg-red-500 px-[8px] text-[.8rem] ">
                                        Due Today
                                    </p>
                                )}
                            <p
                                className={` ${
                                    ele?.completed
                                        ? "bg-green-500"
                                        : "bg-red-500"
                                } absolute bottom-0 right-0 rounded-br-md text-white  px-[8px] text-[.8rem]`}
                            >
                                {ele?.completed ? "Completed" : "Pending"}
                            </p>
                        </label>
                    ))}
                </div>
            </div>
        </div>
    );
};

export default Tasks;
