import React from "react";
import Chart from "./Chart";
const Analytics = () => {
    return (
        <div className=" mt-8 ">
            <Chart />
            <div className=" h-[20vh] "></div>
        </div>
    );
};

export default Analytics;
