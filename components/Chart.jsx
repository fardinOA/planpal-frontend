import React from "react";
import { Pie } from "react-chartjs-2";
import { useSelector } from "react-redux";
import "chart.js/auto";
const Chart = () => {
    const { allTasks } = useSelector((state) => state.task);
    let completedTask = 0;
    let lowCount = 0;
    let midCount = 0;
    let highCount = 0;
    let doneOnTime = 0;
    // calculate out of stock for all products
    allTasks &&
        allTasks.forEach((item) => {
            if (item.completed) {
                completedTask++;
            }
            if (item.priority == "low") lowCount++;
            if (item.priority == "medium") midCount++;
            if (item.priority == "high") highCount++;

            if (
                item.completed &&
                new Date(item.dueDate).toDateString() >=
                    new Date().toDateString(item.updatedAt)
            )
                doneOnTime++;
        });

    // data and configuration for Pie chart
    const pieState1 = {
        labels: ["Cpmpleted", "Incomplete"],
        datasets: [
            {
                data: [
                    completedTask,
                    allTasks && allTasks.length - completedTask,
                ],
                backgroundColor: ["#FFAEBC", "#B4F8C8"],
                hoverBorderColor: ["#FFAEBC", "#B4F8C8"],
                hoverBackgroundColor: ["#FFAEBC", "#B4F8C8"],
                hoverBorderWidth: 8,
                offset: 30,
                borderRadius: 10,
            },
        ],
    };

    const pieState2 = {
        labels: ["Low", "Medium", "High"],
        datasets: [
            {
                data: [lowCount, midCount, highCount],
                backgroundColor: ["#e884c8", "#5a72d2", "#c5da0d"],
                hoverBorderColor: ["#e884c8", "#5a72d2", "#c5da0d"],
                hoverBackgroundColor: ["#e884c8", "#5a72d2", "#c5da0d"],
                hoverBorderWidth: 8,
                offset: 30,
                borderRadius: 10,
            },
        ],
    };

    const pieState3 = {
        labels: ["Done On Time", "Lazy"],
        datasets: [
            {
                data: [doneOnTime, allTasks && allTasks.length - doneOnTime],
                backgroundColor: ["#33c1b5", "#e2e782"],
                hoverBorderColor: ["#33c1b5", "#e2e782"],
                hoverBackgroundColor: ["#33c1b5", "#e2e782"],
                hoverBorderWidth: 8,
                offset: 30,
                borderRadius: 10,
            },
        ],
    };
    // Options for Pie Chart
    const options = {
        cutoutPercentage: 60,
        responsive: true,
        legend: {
            display: true,
            position: "right",
        },

        //custom tooltip
        tooltips: {
            callbacks: {
                title: (items, data) => {
                    let x = Math.ceil(
                        data.datasets[items[0].datasetIndex].data[
                            items[0].index
                        ]
                    );
                    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
                },
                label: (items, data) => data.labels[items.index],
            },

            backgroundColor: "#fff",
            borderColor: "rgb(0,0,0)",
            titleFontSize: 14,
            titleFontColor: "#000",
            bodyFontColor: "#000",
            bodyFontSize: 10,
            displayColors: false,
        },
    };
    return (
        <div className=" mx-auto   ">
            <h1 className=" text-center text-[1.4rem] font-bold ">
                Task Ratio
            </h1>
            <div className=" flex justify-center mx-auto w-[300px] md:w-[400px] ">
                <Pie data={pieState1} options={options} />
            </div>

            <h1 className=" text-center text-[1.4rem] font-bold ">
                Task Priority
            </h1>
            <div className=" flex justify-center mx-auto w-[300px] md:w-[400px] ">
                <Pie data={pieState2} options={options} />
            </div>

            <h1 className=" text-center text-[1.4rem] font-bold ">
                On Time Ratio
            </h1>
            <div className=" flex justify-center mx-auto w-[300px] md:w-[400px] ">
                <Pie data={pieState3} options={options} />
            </div>
        </div>
    );
};

export default Chart;
