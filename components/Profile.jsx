import React from "react";
import { useSelector } from "react-redux";

const Profile = () => {
    const { user } = useSelector((state) => state.user.user);

    return (
        <div className=" p-4 ">
            <h1 className=" text-[1.5rem] font-bold text-center">My Profile</h1>
            <div className=" my-8 flex mx-auto justify-center border rounded-full h-16 w-16 ">
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth="1.5"
                    stroke="currentColor"
                    className="w-9 h-9 mt-3 "
                >
                    <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M15.75 6a3.75 3.75 0 11-7.5 0 3.75 3.75 0 017.5 0zM4.501 20.118a7.5 7.5 0 0114.998 0A17.933 17.933 0 0112 21.75c-2.676 0-5.216-.584-7.499-1.632z"
                    />
                </svg>
            </div>
            <div className=" flex flex-col justify-between gap-9 ">
                <div className=" w-[20rem] p-4 mx-auto bg-gray-200 border shadow-md rounded-md ">
                    <h1 className="  ">Name</h1>
                    <p className=" font-bold text-[1.2rem] ">
                        {user?.firstName} {user?.lastName}{" "}
                    </p>
                </div>
                <div className=" w-[20rem] p-4 mx-auto bg-gray-200 border shadow-md rounded-md ">
                    <h1 className="  ">Email</h1>
                    <p className=" font-bold text-[1.2rem] ">{user?.email} </p>
                </div>
                <div className=" w-[20rem] p-4 mx-auto bg-gray-200 border shadow-md rounded-md ">
                    <h1 className="  ">Joined At</h1>
                    <p className=" font-bold text-[1.2rem] ">
                        {new Date(user?.createdAt).toDateString().slice(4)}
                    </p>
                </div>
            </div>
        </div>
    );
};

export default Profile;
