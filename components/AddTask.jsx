import { useFormik } from "formik";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { createTask, getTasks } from "../redux/features/task/taskSlice";
import { useRouter } from "next/router";

const AddTask = () => {
    const dispatch = useDispatch();
    const { user } = useSelector((state) => state.user);
    const router = useRouter();
    const formik = useFormik({
        initialValues: {
            title: "",
            description: "",
            dueDate: null,
            priority: "low",
        },
        onSubmit: async (values) => {
            console.log(values);
            await dispatch(createTask({ ...values, user: user?.user._id }));
            await dispatch(getTasks());
        },

        validate: (values) => {
            let errors = {};
            if (!values.title) {
                errors.title = "required";
            }
            if (!values.priority) {
                errors.priority = "required";
            }
            if (!values.dueDate) {
                errors.dueDate = "required";
            }

            return errors;
        },
    });
    return (
        <div>
            {" "}
            <input type="checkbox" id="my-modal-4" className="modal-toggle" />
            <label htmlFor="my-modal-4" className="modal cursor-pointer">
                <label className="modal-box relative" htmlFor="">
                    <h3 className="text-lg font-bold">
                        Never Miss a Task Again
                    </h3>
                    <form
                        onSubmit={formik.handleSubmit}
                        className="mt-8 grid grid-cols-6 gap-6"
                    >
                        <div className="col-span-6">
                            <label
                                htmlFor="title"
                                className="block text-sm font-medium text-gray-700"
                            >
                                Title
                            </label>

                            <input
                                onChange={formik.handleChange}
                                type="text"
                                id="title"
                                name="title"
                                className="mt-1 w-full rounded-md border-2 py-1 px-2 bg-white text-sm  focus:outline-none shadow-sm"
                            />
                            <p className=" text-red-500 text-[10px] px-2 ">
                                {formik.errors.title}
                            </p>
                        </div>
                        <div className="col-span-6  ">
                            <label
                                htmlFor="description"
                                className="block text-sm font-medium text-gray-700"
                            >
                                Description
                            </label>

                            <textarea
                                onChange={formik.handleChange}
                                type="text"
                                id="description"
                                name="description"
                                className="mt-1 w-full rounded-md border-2 py-1 px-2 bg-white text-sm  focus:outline-none shadow-sm"
                            />
                        </div>
                        <div className="col-span-6">
                            <label
                                htmlFor="title"
                                className="block text-sm font-medium text-gray-700"
                            >
                                Due Date
                            </label>

                            <input
                                onChange={formik.handleChange}
                                type="date"
                                id="dueDate"
                                name="dueDate"
                                className="mt-1 w-full rounded-md border-2 py-1 px-2 bg-white text-sm  focus:outline-none shadow-sm"
                            />
                            <p className=" text-red-500 text-[10px] px-2 ">
                                {formik.errors.dueDate}
                            </p>
                        </div>

                        <div className="col-span-6">
                            <label
                                htmlFor="title"
                                className="block text-sm font-medium text-gray-700"
                            >
                                Priority
                            </label>
                            <select
                                onChange={formik.handleChange}
                                className="mt-1 rounded-md border-2 py-1 px-2 bg-white text-sm  focus:outline-none shadow-sm"
                                name="priority"
                                id="priority"
                            >
                                <option value="low">Low</option>
                                <option value="medium">Medium</option>
                                <option value="high">High</option>
                            </select>

                            <p className=" text-red-500 text-[10px] px-2 ">
                                {formik.errors.priority}
                            </p>
                        </div>

                        <div className="col-span-6 sm:flex sm:items-center sm:gap-4">
                            <button
                                type="submit"
                                className="inline-block w-full shrink-0 rounded-md border border-blue-600 bg-blue-600 px-12 py-3 text-sm font-medium text-white transition hover:bg-transparent hover:text-blue-600 focus:outline-none focus:ring active:text-blue-500"
                            >
                                Create
                            </button>
                        </div>
                    </form>
                </label>
            </label>
        </div>
    );
};

export default AddTask;
