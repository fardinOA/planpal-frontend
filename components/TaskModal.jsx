import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { deleteTask, taskDone } from "../redux/features/task/taskSlice";
import { useRouter } from "next/router";

const TaskModal = () => {
    const { selectedTask } = useSelector((state) => state.task);
    const router = useRouter();
    const dispatch = useDispatch();

    const doneHandeler = (e) => {
        dispatch(taskDone(selectedTask?._id));
    };

    return (
        <div>
            <input type="checkbox" id="my-modal-5" className="modal-toggle" />
            <label htmlFor="my-modal-5" className="modal cursor-pointer">
                <label className="modal-box relative" htmlFor="">
                    <div className=" flex justify-between">
                        <h3 className="text-[1.5rem] font-bold">
                            {selectedTask?.title}
                        </h3>

                        <button
                            onClick={() => {
                                dispatch(deleteTask(selectedTask?._id));
                                router.reload();
                            }}
                        >
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 24 24"
                                strokeWidth={1.5}
                                stroke="currentColor"
                                className="w-6 h-6 text-[tomato] "
                            >
                                <path
                                    strokeLinecap="round"
                                    strokeLinejoin="round"
                                    d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0"
                                />
                            </svg>
                        </button>
                    </div>
                    <p className="py-4">{selectedTask?.description}</p>
                    <p> Priority: {selectedTask?.priority}</p>
                    {selectedTask?.completed ? (
                        <p className=" text-green-500 w-fit    rounded-sm ">
                            Completed
                        </p>
                    ) : (
                        <p className=" bg-[#f77963] border-none rounded-sm w-fit text-white px-2 ">
                            Due:{" "}
                            {new Date(selectedTask?.dueDate).toDateString()}
                        </p>
                    )}
                    <p>
                        Created At:{" "}
                        {new Date(selectedTask?.createdAt).toDateString()}{" "}
                    </p>
                    {!selectedTask?.completed && (
                        <p
                            onClick={doneHandeler}
                            className=" cursor-pointer hover:bg-blue-600 bg-blue-500 text-center text-white mt-2 rounded-sm "
                        >
                            Mark As Done
                        </p>
                    )}
                </label>
            </label>
        </div>
    );
};

export default TaskModal;
