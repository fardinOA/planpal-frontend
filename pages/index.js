import { useRouter } from "next/router";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Loader from "../components/Loader";

import AddTask from "../components/AddTask";
import { getTasks } from "../redux/features/task/taskSlice";
import axios from "../utils/axios";
import { wrapper } from "../redux/store";
import { getLoginData, loadLoginUser } from "../redux/features/user/userSlice";
import Tasks from "../components/Tasks";
import TaskModal from "../components/TaskModal";

export default function Home({ data }) {
    const router = useRouter();
    const dispatch = useDispatch();
    const { isAuth, isLoading, user } = useSelector((state) => state.user);
    useEffect(() => {
        dispatch(getTasks());
    }, []);

    if (data) {
        dispatch(getLoginData(data));
    }

    return (
        <>
            {isLoading ? (
                <Loader />
            ) : (
                <div className="  ">
                    <Tasks />
                    <div className=" h-[30vh] "></div>
                    Home
                    {/* Popup modal for creating todo */}
                    {/* <AddTask /> */}
                    <TaskModal />
                </div>
            )}
        </>
    );
}

export const getServerSideProps = wrapper.getServerSideProps(
    (store) =>
        async ({ req }) => {
            try {
                const config = {
                    headers: {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Headers": "*",
                        Cookie: req.headers?.cookie,
                    },
                };
                axios.defaults.withCredentials = true;
                const { data } = await axios.get("/plan/v1/me", config);
                if (!data?.success) {
                    return {
                        redirect: {
                            permanent: false,
                            destination: "/login",
                        },
                        props: {},
                    };
                }

                return {
                    props: { data },
                };
            } catch (error) {
                return {
                    redirect: {
                        permanent: false,
                        destination: "/login",
                    },
                    props: {
                        // error: error.response?.data,
                    },
                };
            }
        }
);
