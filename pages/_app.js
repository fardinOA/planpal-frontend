import { Provider, useDispatch, useSelector } from "react-redux";
import "../styles/globals.css";
// import store from "../redux/store";
import "react-toastify/dist/ReactToastify.css";
import { wrapper } from "../redux/store";
import { ToastContainer } from "react-toastify";
import { useEffect } from "react";
import { loadLoginUser } from "../redux/features/user/userSlice";
import { useRouter } from "next/router";
import Layout from "../components/Layout";
import AddTask from "../components/AddTask";
function MyApp({ Component, ...rest }) {
    const { store, props } = wrapper.useWrappedStore(rest);

    useEffect(() => {
        // store.dispatch(loadLoginUser());
    }, []);

    return (
        <Provider store={store}>
            <Layout>
                <AddTask />
                <Component {...props.pageProps} />
                <ToastContainer
                    position="top-center"
                    autoClose={5000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                    theme="light"
                />
            </Layout>
        </Provider>
    );
}

// export default wrapper.withRedux(MyApp);
export default MyApp;
