import React, { useEffect, useReducer } from "react";
import Profile from "../components/Profile";
import { useSelector } from "react-redux";
import { useRouter } from "next/router";
import { wrapper } from "../redux/store";

const profile = () => {
    const { isAuth } = useSelector((state) => state.user);
    const router = useRouter();

    useEffect(() => {
        if (!isAuth) {
            router.push("/login");
        }
    }, [isAuth]);

    return <Profile />;
};

export default profile;
