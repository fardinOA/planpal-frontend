import axios from "axios";

const axiosInstance = axios.create({
    baseURL: "http://localhost:4000",
    // baseURL: "https://planpal-backend.vercel.app",
});

export default axiosInstance;
